# ansible_role_reverse_proxy
Role deploys docker container with nginx reverse proxy.

[jwilder/nginx-proxy](https://hub.docker.com/r/jwilder/nginx-proxy) docker container is used as nginx reverse proxy.

[nginxproxy/acme-companion](https://hub.docker.com/r/nginxproxy/acme-companion) docker container is used as nginx proxy companion container for creation and renewal of SSL certificates for proxied Docker containers. 

## Requirements

Tested on:
Debian 10, 11;
Ubuntu 20.04, 22.04

## Role Variables

### defaults/main.yml
Choose whether to generate SSL certificates for proxied Docker containers' domains.
```
reverse_proxy_generate_certs: true
```
Choose name of nginx-proxy container.
```
reverse_proxy_container_name: nginx-proxy
```
Set your email address to receive notifications from Let's Encrypt about expiring certificates.
```
reverse_proxy_cert_email: mail@yourdomain.tld
```
## Environment variables that should be set in proxied Docker containers

```
VIRTUAL_HOST: subdomain.domain.xyz          # proxied container should be reachable at this address
LETSENCRYPT_HOST: subdomain.domain.xyz      # certificate is issued for this domain name
VIRTUAL_PORT: "port"                        # port should be set if proxied container listens another port than the default 80

```
## License

BSD-3-Clause
